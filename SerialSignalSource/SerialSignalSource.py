import serial, math, time 
from enum import Enum

powerLevels ={
    "-4dB": 1,
    "-1dB": 2,
    "+2dB": 3,
    "+5dB": 4
}


class SerialSignalClass:
    def __init__(self, serialPort, baudrate=115200, debug = 0, normalStart = 0):
        self.debug = debug
        self.ser = serial.Serial(serialPort, baudrate)
        self.ser.flushInput()

    def setFrequency(self, freq, power="-4dB"):
        fractional, whole = math.modf(float(freq))
        ByteArray = bytearray(9)
        ByteArray[0] = 0x55
        ByteArray[1] = 0x55
        ByteArray[2] = int(whole) >> 8
        ByteArray[3] = int(whole) & 0xFF 
        ByteArray[4] = int(fractional * 1000) >> 8 
        ByteArray[5] = int(fractional * 1000) & 0xFF
        ByteArray[6] = powerLevels[power]
        ByteArray[7] = 0x0D
        ByteArray[8] = 0x0A
        self.ser.write(ByteArray)

    def sweepRange(self, start=23.5, stop=6000.0, step=1.0, power="-4dB", timeStep = 0.5):
        i = float(start)
        if start < stop:
            while(i <= stop):
                if(i > 23.5 and i < 6000.0): 
                    self.setFrequency(i, power)
                elif(i > 6000.0):
                    self.setFrequency(6000, power)
                    return("Maximum reached")
                elif(i < 23.5):
                    self.setFrequency(23.5, power)
                    return("Minimum reached")
                time.sleep(timeStep)
                i+=float(step)
        else:
            while(i >= stop):
                if(i > 23.5 and i < 6000.0): 
                    self.setFrequency(i, power)
                elif(i > 6000.0):
                    self.setFrequency(6000, power)
                    return("Maximum reached")
                elif(i < 23.5):
                    self.setFrequency(23.5, power)
                    return("Minimum reached")
                time.sleep(timeStep)
                i-=float(step)
        return("Finished Sweep")